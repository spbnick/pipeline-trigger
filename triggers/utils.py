"""Generic functions and constants"""
from dataclasses import dataclass, field
import json
import logging
import os
import re
import subprocess

import requests


# Patch name skip patterns. These patches live in kernel Patchworks but don't
# apply to the tree. Sometimes, a feature requires a change both in kernel and
# a tool closely associated with it. These are sent as a single series and
# maintainers know how to deal with them, but we don't want to download them
# because we'd just get patch application errors since the tools are not part
# of the kernel tree. We also aren't interested in pull requests, which also
# can end up in Patchwork.
PATCH_SKIP_PATTERNS = [r'\[[^\]]*iproute.*?\]',
                       r'\[[^\]]*pktgen.*?\]',
                       r'\[[^\]]*ethtool.*?\]',
                       r'\[[^\]]*git.*?\]',
                       r'\[[^\]]*pull.*?\]',
                       r'pull.?request']
PATCH_SKIP = re.compile('|'.join(PATCH_SKIP_PATTERNS), re.IGNORECASE)


class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""
    pass


@dataclass
class SeriesData:
    """
    Class for handling patch series data.

    'last_tested' is customizable by the trigger -- PW2 uses a datetime
    string while PW1 a stringified patch ID.
    """
    patches: list = field(default_factory=list)
    emails: set = field(default_factory=set)
    subject: str = ''
    message_id: str = ''
    last_tested: str = ''
    series_id: str = ''
    cover: str = ''

    def __repr__(self):
        return '{} {}\n{} {}\n{} {}\n{} {}\n{} {}\n{} {}\n{} {}'.format(
            'Data for series:', self.series_id,
            'Message ID:', self.message_id,
            'Subject:', self.subject,
            'Cover letter:', self.cover,
            'Patches:', self.patches,
            'Last tested:', self.last_tested,
            'Retrieved emails:', self.emails
        )


def get_commit_hash(repository, branch):
    """
    Return current commit for the branch in question.

    Args:
        repository: Git repository URL.
        branch:     Git reference to test.
    """
    logging.debug('Getting the last ref from %s@%s', repository, branch)
    commithash, _ = subprocess.check_output(
        ['git', 'ls-remote', '--heads', repository, branch]
    ).split()
    return commithash.decode('utf-8')


def get_env_var_or_raise(name):
    """
    Retrieve the value of an environment variable. Raise EnvVarNotSetError if
    it's not set.

    Args:
        name:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.
    """
    env_var = os.getenv(name)
    if not env_var:
        raise EnvVarNotSetError(f'Environment variable {name} is not set!')
    return env_var


def get_variables(project, pipeline):
    """
    Get the variables used by the corresponding job. Use a no so disgusting
    hack because python-gitlab doesn't expose this attribute yet.

    Args:
        project:  Object representing the GitLab project.
        pipeline: Object representing the pipeline to retrieve variables for

    Returns:
        A dictionary representing job's variables.
    """
    try:
        job_id = pipeline.jobs.list(page=1, per_page=1)[0].attributes['id']
    except IndexError:
        # pipeline without any job associated (e.g. corrupted yaml)
        return {}

    job_url = '{}/-/jobs/{}.json'.format(project.attributes.get('web_url'),
                                         job_id)
    response = requests.get(job_url)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, job_url
        ))

    try:
        variables = json.loads(response.content)['trigger']['variables']
    except KeyError:
        return {}
    result = {}
    for var in variables:
        result[var['key']] = var['value']
    return result


def create_commit(project, trigger, title):
    """
    Create a commit in the branch where the pipeline will run. It's just used
    for getting an intuitive view using the gitlab interface.

    Args:
        project: GitLab project to create commit in.
        trigger: Dictionary with all data to include in the message.
        title:   String to use as the commit message title.
    """
    commit_message = title + '\n'
    for key, value in trigger.items():
        commit_message += '\n{} = {}'.format(key, value)
    data = {
        'branch': trigger['cki_pipeline_branch'],
        'commit_message': commit_message,
        'actions': [],
    }
    project.commits.create(data)


def was_tested(project, branch, pipeline_id, pages=50):
    """
    Find out if the pipeline was already tested or not.

    Args:
        project:     GitLab project which the pipelines are associated with.
        branch:      Project's branch responsible for testing of the repo.
        pipeline_id: ID of the pipeline we want to check.
        pages:       How many past pages of pipelines to check. Defaults to 50.

    Returns:
        True if the pipeline with given pipeline_id was already tested, False
        otherwise.
    """
    for page in range(1, pages + 1):
        logging.info('Fetching page %d/%d', page, pages)
        for pipeline in project.pipelines.list(page=page, per_page=10):
            if pipeline.attributes['ref'] == branch:
                variables = get_variables(project, pipeline)
                if variables.get('cki_pipeline_id') == pipeline_id:
                    return True

    return False


def trigger_pipelines(gitlab_instance, trigger_token, triggers):
    """
    Trigger pipelines.

    Args:
        gitlab_instance: gitlab.Gitlab object reporesenting GitLab instance.
        trigger_token:   Token to trigger the pipeline with.
        triggers:        List of dictionaries describing the pipeline to
                         trigger. The dictionary contains pipeline variables.
    """
    for index, trigger in enumerate(triggers):
        project = gitlab_instance.projects.get(trigger['cki_project'])
        create_commit(project, trigger, trigger['title'])
        project.trigger_pipeline(trigger['cki_pipeline_branch'],
                                 trigger_token,
                                 trigger)
        logging.info('Pipeline %d/%d for %s triggered',
                     index + 1,
                     len(triggers),
                     trigger['cki_pipeline_branch'])
