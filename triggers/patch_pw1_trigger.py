"""
Pipeline trigger for Patchwork1 patches.

1. Find the last pipeline for chosen project and retrieve the ID of last tested
   patch.
2. Get new series. This is super tricky because PW1 doesn't have series
   detection. First we retrieve all new patches and restore their subjects.
   Then we check the subjects for patch number in series and total expected
   patches. If they are missing, we got a single patch. Otherwise we are
   dealing with a patch series. Since we don't have a series ID to use, we
   mock up one and save the data. Repeat this for all the retrieved patches.
3. For each reconstucted series, check if all the patches are present and if
   so, put the data together.
4. Profit $$$.
"""
import base64
from collections import OrderedDict
import email
import email.header
import email.parser
import email.utils
import logging
import re
import urllib.parse
import xmlrpc.client

import requests

from . import utils


RPC_FIELDS = ['id', 'name', 'submitter', 'msgid',
              ['root_comment', ['headers']]]


class RPCWrapper():
    """Wrapper for the RPC interface, because PW1 fork is special."""
    def __init__(self, real_rpc):
        self.rpc = real_rpc

    def __getattr__(self, name):
        function = getattr(self.rpc, name)

        def wrapper(*args, **kwargs):
            # Use the (currently) newest version of PW1 API
            _, result = function(1019, *args, **kwargs)
            return result

        return wrapper


class BasicHTTPAuthTransport(xmlrpc.client.Transport):
    def __init__(self, username, password):
        self.username = username
        self.password = password
        super(BasicHTTPAuthTransport, self).__init__()

    def send_headers(self, connection, headers):
        for key, val in headers:
            connection.putheader(key, val)

        credentials = '{}:{}'.format(self.username, self.password)
        auth = b'Basic ' + base64.b64encode(credentials.encode()).strip()
        connection.putheader('Authorization', auth)


def sanitize_emails(patch_objects):
    """
    Retrieve From, To and Cc header values for given patch objects.

    Args:
        patch_objects: A list of patch objects, as returned by XMLRPC. Both
                       actual patches and cover letters are patch objects in
                       Patchwork1.

    Returns:
        A set of sanitized and decoded unique email addresses.
    """
    email_headers = ['From', 'To', 'Cc']
    emails = set()

    for patch in patch_objects:
        headers = email.parser.Parser().parsestr(
            patch['root_comment']['headers'],
            True
        )

        for key in email_headers:
            if key in headers:
                unfolded = re.sub(r'\r?\n[ \t]', ' ', headers[key])
                email_data = email.utils.getaddresses([unfolded])
                for _, address in email_data:
                    emails.add(address)

    return emails


def process_series_data(series_data, patchwork_url):
    """
    Process the dictionaries representing series data and return completed
    series.

    Args:
        series:        A list of dictionaries representing the data.
        patchwork_url: Base URL of the Patchwork instance.

    Returns:
        A list of utils.SeriesData objects.
    """
    series_list = []

    for series in series_data:
        total = series['total']
        if total == len(series['patches']):
            patches = ['{}/patch/{}'.format(patchwork_url,
                                            series['patches'][index]['id'])
                       for index in range(1, total + 1)]

            series_data = utils.SeriesData(
                patches=patches,
                subject=series['patches'][total]['name'],
                message_id=series['patches'][total]['msgid'],
                last_tested=str(series['patches'][total]['id'])
            )

            emails = sanitize_emails(list(series['patches'].values()))
            if series['cover']:
                series_data.cover = '{}/patch/{}/mbox'.format(
                    patchwork_url, series['cover']['id']
                )
                emails = emails | sanitize_emails([series['cover']])

            series_data.emails = emails

            logging.debug('%s', series_data)
            series_list.append(series_data)

    return series_list


def get_series_by_patch(all_patches, patchwork_url):
    """
    Retrieve new series data based on all the new patches.

    Args:
        all_patches:   A list of patch objects, as returned by XMLRPC.
        patchwork_url: URL of the Patchwork1 instance.

    Returns:
        A list of utils.SeriesData objects.
    """
    # Mock up series and series IDs. We'll populate the dictionary structure:
    # {'series_id': {'total': X, 'cover': None or object,
    #  'patches': {1-X: patch or None}}}
    # Only completed series will be processed further.
    seen_series = OrderedDict()

    for patch in all_patches:
        # No worries here about skipping a patch. We don't expect internal
        # people sending tooling patches to this kernel list.
        if utils.PATCH_SKIP.search(patch['name']):
            continue

        # Extract series data from the patch subject
        series_match = re.search(r'\[.*?(\d+)/(\d+).*?\]', patch['name'])
        if series_match:
            # We got patch series here
            current_patch_number = int(series_match.group(1))
            number_of_patches = int(series_match.group(2))

            id_match = re.match(r'\<(\d+\W\d+)\W\d+.*@', patch['msgid'])
            if id_match:
                series_id = '{}_{}'.format(number_of_patches,
                                           id_match.group(1))
            else:
                # Mock up a series ID from the submitter and number of patches
                # in the series and hope there are only one such series.
                series_id = '{}_{}'.format(patch['submitter_id'],
                                           number_of_patches)

            if series_id not in seen_series:
                seen_series[series_id] = {'total': number_of_patches,
                                          'cover': None,
                                          'patches': {}}

            if current_patch_number == 0:
                seen_series[series_id]['cover'] = patch
            else:
                seen_series[series_id]['patches'][current_patch_number] = patch
        else:
            # We got a single patch without cover
            seen_series[patch['name']] = {'total': 1,
                                          'cover': None,
                                          'patches': {1: patch}}

    series_list = process_series_data(list(seen_series.values()),
                                      patchwork_url)
    return series_list


def restore_patch_name(patch):
    """
    Restore the patch subject. PW1 fork doesn't use the email subject as the
    patch name and we need the subject to reconstruct the series.

    Args:
        patch: A patch object, as returned by the XMLRPC.
    """
    headers = email.parser.Parser().parsestr(patch['root_comment']['headers'],
                                             True)
    subject = headers.get('Subject', '').replace('\n\t', ' ').replace('\n', '')
    patch['name'] = email.header.decode_header(subject)[0][0]


def get_new_series(rpc, project_name, last_patch, patchwork_url):
    """
    Helper function to retrieve all new series. Wrapper around recursive
    get_series_by_patch().

    Args:
        rpc:           XMLRPC connection object to the Patchwork instance.
        project_name:  Name of the Patchwork project to query.
        last_patch:    ID of the last tested patch.
        patchwork_url: URL of the Patchwork1 instance.

    Returns:
        A list of dictionaries representing all new series.

    Raises:
        Exception if no such project exists on the Patchwork instance.
    """
    # PW1 fork makes case-insensitive search, not "equals". So we may very well
    # get a bunch of "rhel7", "rhel76", "rhel7-fs" projects back.
    possible_projects = rpc.project_list(project_name)
    real_project_id = next((project['id'] for project in possible_projects if
                            project['linkname'] == project_name), None)
    # We may have ended up with no project we actually wanted
    if not real_project_id:
        raise Exception('No project {} found!'.format(project_name))

    all_patches = rpc.patch_list(
        {'project_id': real_project_id, 'id__gt': last_patch},
        False,
        RPC_FIELDS
    )
    # Restore the email subjects
    for patch in all_patches:
        restore_patch_name(patch)

    return get_series_by_patch(all_patches, patchwork_url)


def get_last_patch(project, data, pages=200):
    for current_page in range(1, pages + 1):
        logging.info('Fetching page %d/%d', current_page, pages)
        for pipeline in project.pipelines.list(page=current_page, per_page=10):
            if pipeline.attributes['ref'] == data['cki_pipeline_branch']:
                variables = utils.get_variables(project, pipeline)
                if variables.get('cki_pipeline_type') == 'patchwork-v1':
                    url = variables.get('patchwork_url')
                    patchwork_project = variables.get('patchwork_project')
                    if (url, patchwork_project) != (
                            data['patchwork_url'], data['patchwork_project']):
                        # We found the last patch from a different source
                        continue

                    patch_id = variables.get('last_patch_id')
                    if patch_id:
                        logging.info('Last tested patch was %s', patch_id)
                        return int(patch_id)

    raise Exception(
        'No tested patches found in the last {} pages!'.format(pages)
    )


def get_cookie(user, password, patchwork_url):
    """
    Log in as the Patchwork user and retrieve the cookie. This is passed along
    so the pipeline itself can use it to retrieve the patches. The cookie lasts
    for ~2weeks so we can do this without fear.

    Args:
        user:          Patchwork username.
        password:      Patchwork password.
        patchwork_url: Base URL of the Patchwork instance.

    Returns:
        Retrieved session_id.
    """
    user_data = urllib.parse.urlencode({'username': user,
                                        'password': password})
    cookie_session = requests.session()
    with cookie_session as session:
        session.post(urllib.parse.urljoin(patchwork_url, 'accounts/login/'),
                     data=user_data)

    return cookie_session.cookies.get_dict().get('sessionid')


def load_triggers(gitlab_instance, patches_config):
    triggers = []
    for key, value in patches_config.items():
        value = value.copy()
        cki_project = gitlab_instance.projects.get(value['cki_project'])
        baseline_project = gitlab_instance.projects.get(
            value['baseline_project']
        )

        # Translate the human-friendly yaml terminology to skt and remove
        # nesting, just so we don't have to deal with it later when creating
        # the commit and triggering the pipeline.
        value['baserepo'] = value['git_url']
        del value['git_url']
        value['patchwork_url'] = value['patchwork']['url']
        value['patchwork_project'] = value['patchwork']['project']
        del value['patchwork']

        if not value.get('make_target'):
            value['make_target'] = 'targz-pkg'

        value['cki_pipeline_type'] = 'patchwork-v1'
        value['name'] = key
        value['ref'] = utils.get_commit_hash(value['baserepo'],
                                             value['branch'])
        last_patch = get_last_patch(cki_project, value)

        # Initialize the XMLRPC connection and log in
        user = utils.get_env_var_or_raise('PATCHWORK_USER')
        password = utils.get_env_var_or_raise('PATCHWORK_PASSWORD')
        rpc = RPCWrapper(xmlrpc.client.ServerProxy(
            '{}/xmlrpc/'.format(value['patchwork_url'].strip('/')),
            transport=BasicHTTPAuthTransport(user, password)
        ))

        new_series = get_new_series(rpc,
                                    value['patchwork_project'],
                                    last_patch,
                                    value['patchwork_url'])

        value['patchwork_session_cookie'] = get_cookie(user,
                                                       password,
                                                       value['patchwork_url'])

        for series in new_series:
            trigger = value.copy()
            trigger['patchwork'] = ' '.join(series.patches)
            if trigger.get('send_report_to_upstream', False):
                trigger['mail_to'] = ', '.join(series.emails)
            # GitLab only allows strings
            trigger['send_report_to_upstream'] = str(
                trigger.get('send_report_to_upstream', False)
            )
            trigger['send_report_on_success'] = str(
                trigger.get('send_report_on_success', False)
            )
            trigger['message_id'] = series.message_id
            trigger['subject'] = 'Re: ' + series.subject
            trigger['last_patch_id'] = series.last_tested
            trigger['cover_letter'] = series.cover
            trigger['title'] = 'Patch: {}: {}'.format(trigger['name'],
                                                      series.subject)
            triggers.append(trigger)
    return triggers
