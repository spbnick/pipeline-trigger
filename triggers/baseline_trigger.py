"""
Pipeline trigger for baselines.

1. Get current top commit of the requested repo/branch.
2. Check if it was already tested.
3. Profit $$$.
"""
import copy
import logging
import hashlib

from . import utils


def generate_hash(git_url, branch, commit):
    """
    Generate a sha256 hash used as cki_pipeline_id

    Args:
        git_url: URL of the git repo being tested.
        branch:  Git branch being tested.
        commit:  Git commit which is being tested.

    Returns:
        A sha256 digest of formatted string of arguments.
    """
    string = f'{git_url}@{branch}:{commit}'
    return hashlib.sha256(string.encode('utf-8')).hexdigest()


def load_triggers(gitlab_instance, baselines_config):
    """
    Get ready all potential triggers.
    """
    triggers = []
    for key, value in baselines_config.items():
        for branch in value['branches']:
            trigger = copy.deepcopy(value)
            project = gitlab_instance.projects.get(trigger['cki_project'])

            del trigger['branches']
            trigger['ref'] = utils.get_commit_hash(trigger['git_url'], branch)
            trigger['cki_pipeline_id'] = generate_hash(trigger['git_url'],
                                                       branch,
                                                       trigger['ref'])
            trigger['branch'] = branch
            trigger['name'] = key
            trigger['cki_pipeline_type'] = 'baseline'
            # GitLab only allows strings
            trigger['require_manual_review'] = str(
                trigger.get('require_manual_review', 'True')
            )

            if not trigger.get('make_target'):
                trigger['make_target'] = 'targz-pkg'

            # skt uses 'baserepo' instead of 'git_url'
            trigger['baserepo'] = trigger['git_url']
            del trigger['git_url']

            do_not_trigger = utils.was_tested(project,
                                              trigger['cki_pipeline_branch'],
                                              trigger['cki_pipeline_id'])

            if do_not_trigger:
                logging.info('Pipeline for %s@%s already triggered.',
                             trigger['branch'],
                             trigger['ref'])
            else:
                trigger['title'] = 'Baseline: {} {}@{}'.format(
                    trigger['name'],
                    trigger['branch'],
                    trigger['ref']
                )
                triggers.append(trigger)
    return triggers
