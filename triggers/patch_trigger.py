"""
Pipeline trigger for patches from Patchwork2 instances.

1. Find the last pipeline for chosen project and get the 'date' and
   'skipped_series' variables. 'date' is used in the PW2 API call to get new
   patches since 'date' and 'skipped_series' are series which were skipped in
   the previous runs because the cover letter was delayed.
2. Get needed data about both previously skipped series and the new ones.
3. Check if any of the series need to be delayed. Save the info about them
   into the newly constructed 'skipped_series' and skip them.
4. Profit $$$.
"""
import datetime
import email.utils
import logging
import os
import re
from urllib.parse import quote

import dateutil.parser
import requests

from . import utils


def sanitize_emails(headers):
    """
    Retrieve From, To and Cc header values for given patch.

    Args:
        headers: A list of patch's email headers, grabbed from Patchwork's API
                 response. patch['headers']

    Returns:
        A set of sanitized and decoded unique email addresses.
    """
    email_headers = ['From', 'To', 'Cc']
    emails = set()

    for key in email_headers:
        if key in headers:
            unfolded = re.sub(r'\r?\n[ \t]', ' ', headers[key])
            email_data = email.utils.getaddresses([unfolded])
            for _, address in email_data:
                emails.add(address)

    return emails


def get_patch_data(patchwork_url, patch_web_url):
    """
    Get the data associated with the patch. These are used in various parts of
    the pipeline.

    Args:
        patchwork_url: Base URL of the Patchwork instance.
        patch_web_url: Direct URL of the patch.

    Returns:
        3 strings representing the message ID, subject and date of the patch.
    """
    patch_id = os.path.basename(patch_web_url.rstrip('/'))

    response = requests.get('{}/api/patches/{}'.format(patchwork_url,
                                                       patch_id))
    if response.status_code != 200:
        raise Exception(
            'Unexpected response {} from {}/api/patches/{} !'.format(
                response.status_code, patchwork_url, patch_id
            ))

    patch_data = response.json()

    # PW API is funny sometimes and both 'Message-Id' and 'Message-ID' are
    # valid keys
    try:
        message_id = patch_data['headers']['Message-Id']
    except KeyError:
        message_id = patch_data['headers']['Message-ID']

    subject = re.sub(r'\r?\n[ \t]', ' ', patch_data['headers']['Subject'])
    date = patch_data['date']

    return message_id, subject, date


def get_series_data(patchwork_url, series_url):
    """
    Get a list of patches from the series and associated data we need.

    Args:
        patchwork_url: Base URL of the Patchwork instance.
        series_url:    URL of the series detail in the Patchwork API.

    Returns:
        A utils.SeriesData object.
    """
    patches = []
    emails = set()

    response = requests.get(series_url)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, series_url
        ))

    series_data = response.json()
    if not series_data['received_all'] or \
            utils.PATCH_SKIP.search(series_data['name']):
        logging.info('Skipping %d: %s', series_data['id'], series_data['name'])
        return None

    for patch in series_data['patches']:
        if not utils.PATCH_SKIP.search(patch['name']):
            patches.append(patch['web_url'])

            # Nested patch objects in series detail don't contain headers so we
            # need to get the actual patch detail...
            patch_response = requests.get('{}/api/patches/{}'.format(
                patchwork_url, patch['id']
            ))
            if patch_response.status_code != 200:
                raise Exception(
                    'Unexpected response {} when getting patch {} !'.format(
                        patch_response.status_code, patch['id']
                    )
                )
            patch_detail = patch_response.json()
            emails |= sanitize_emails(patch_detail['headers'])

    if not patches:
        return None

    message_id, subject, last_date = get_patch_data(patchwork_url,
                                                    patches[-1])
    retrieved_series = utils.SeriesData(
        patches=patches,
        emails=emails,
        subject=subject,
        message_id=message_id,
        last_tested=last_date,
        series_id=str(series_data['id'])
    )
    if series_data['cover_letter']:
        retrieved_series.cover = series_data['cover_letter']['mbox']

    logging.debug('%s', retrieved_series)
    return retrieved_series


def get_series_by_patch(patchwork_url, patch_endpoint, seen=set()):
    """
    Retrieve a list of new series data. Check out new patches from the patch
    API endpoint instead of checking the series endpoint to make sure nothing
    gets missed -- sometimes people post subsequent patches to already existing
    series and we do want to retest them, but these would be missed by querying
    the series endpoint.

    Args:
        patchwork_url:  Base URL of the Patchwork instance.
        patch_endpoint: URL to the Patchwork's patch API to use. May contain
                        filtering.
        seen:           A set of already seen series IDs (all patches from the
                        same series have the same series ID so we don't want to
                        query them multiple times). This is used for recursion,
                        don't actually pass any value!

    Returns:
        A list of dictionaries representing all new series.
    """
    series_list = []
    response = requests.get(patch_endpoint)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, patch_endpoint
        ))

    response_data = response.json()
    # If there is a single patch returned we get a dict, not a list with a
    # single element. Fix this inconsistency for easier processing.
    if not isinstance(response_data, list):
        response_data = [response_data]

    for patch in response_data:
        logging.debug('Found patch %d', patch['id'])
        # There can be only one series associated with a patch but it's a list.
        # At least until the "Convert Series-Patch relationship to 1:N" series
        # gets merged, then it might be a dict and we need to verify and fix
        # the solution as needed.
        series_id = patch['series'][0]['id']
        if series_id not in seen:
            logging.info('Discovered new series %d', series_id)
            seen.add(series_id)

            new_series = get_series_data(
                patchwork_url,
                '{}/api/series/{}'.format(patchwork_url, series_id)
            )
            if new_series:
                series_list.append(new_series)

    link = response.headers.get('Link')
    if link:
        match = re.match('<(.*)>; rel="next"', link)
        if match:
            series_list += get_series_by_patch(patchwork_url,
                                               match.group(1),
                                               seen)

    return series_list


def retry_skipped(patchwork_url, skipped_series):
    """
    Requery previously skipped series and return their data.

    Args:
        patchwork_url:  Base URL of the Patchwork instance.
        skipped_series: A list of skipped series IDs.

    Returns: A list of series data for previously skipped series.
    """
    series_list = []
    logging.info('Requerying previously skipped series: %s', skipped_series)
    for series_id in skipped_series:
        series_data = get_series_data(
            patchwork_url, '{}/api/series/{}'.format(patchwork_url, series_id)
        )
        if series_data:
            series_list.append(series_data)

    return series_list


def get_new_series(url, project, since):
    """
    Helper function to retrieve all new series. Wrapper around recursive
    get_series_by_patch().

    Args:
        url:     Base URL of Patchwork instance.
        project: Name of the Patchwork project to query.
        since:   Datetime string since when to retrive new patches.

    Returns:
        A list of dictionaries representing all new series.
    """
    patch_endpoint = '{}/api/patches?project={}&since={}'.format(
        url,
        project,
        quote(since.isoformat())
    )
    return get_series_by_patch(url, patch_endpoint)


def missing_cover(patchwork_url, series):
    """
    Check that if a cover letter was expected, it did already arrive by the
    time we queried the series. Patchwork2 currently offers no way to verify
    this so we must check it manually.

    Args:
        patchwork_url: Base URL of the Patchwork2 instance.
        series:        A dictionary representing series data.

    Returns: True if a cover letter is expected and we don't have it,
             False otherwise.
    """
    if series.cover:
        return False

    first_patch_url = series.patches[0].strip('/')
    api_url = '{}/api/patches/{}'.format(patchwork_url,
                                         first_patch_url.split('/')[-1])
    response = requests.get(api_url)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, api_url
        ))
    response_data = response.json()

    in_reply_to = response_data.get('headers', {}).get('In-Reply-To')
    if not in_reply_to:
        return False

    return True


def get_next_date_and_skipped_series(project, data, pages=200):
    """
    Get the last patch commit for given repo/branch/patchwork data, and
    retrieve the date and skipped series associated with it.

    Because Patchwork's 'since' parameter uses '>=' operation, we have to
    increase the retrieved date by a second -- using the original date, we'd
    test the last series again!

    The skipped series are for re-checking the series for which a cover letter
    didn't arrive when they were originally checked.
    """
    for current_page in range(1, pages + 1):
        logging.info('Fetching page %d/%d', current_page, pages)
        for pipeline in project.pipelines.list(page=current_page, per_page=10):
            if pipeline.attributes['ref'] == data['cki_pipeline_branch']:
                variables = utils.get_variables(project, pipeline)
                if variables.get('cki_pipeline_type') == 'patchwork':
                    url = variables.get('patchwork_url')
                    patchwork_project = variables.get('patchwork_project')
                    if (url, patchwork_project) != (
                            data['patchwork_url'], data['patchwork_project']):
                        # We found the last patch from a different source
                        continue

                    patch_date = variables.get('date')
                    skipped = variables.get('skipped_series', '')
                    if patch_date:
                        logging.info('Last tested patch was from %s',
                                     patch_date)
                        new_date = dateutil.parser.parse(
                            patch_date) + datetime.timedelta(seconds=1)

                        skipped_series = skipped.split()
                        logging.info('Previously skipped series: %s',
                                     skipped_series)
                        return (new_date, skipped_series)

    raise Exception(
        'No tested patches found in the last {} pages!'.format(pages)
    )


def load_triggers(gitlab_instance, patches_config):
    triggers = []
    for key, value in patches_config.items():
        value = value.copy()
        project = gitlab_instance.projects.get(value['cki_project'])

        # Translate the human-friendly yaml terminology to skt and remove
        # nesting, just so we don't have to deal with it later when creating
        # the commit and triggering the pipeline.
        value['baserepo'] = value['git_url']
        del value['git_url']
        value['patchwork_url'] = value['patchwork']['url']
        value['patchwork_project'] = value['patchwork']['project']
        del value['patchwork']

        if not value.get('make_target'):
            value['make_target'] = 'targz-pkg'

        value['cki_pipeline_type'] = 'patchwork'
        value['name'] = key
        value['ref'] = utils.get_commit_hash(value['baserepo'],
                                             value['branch'])
        next_date, skipped_series = get_next_date_and_skipped_series(project,
                                                                     value)
        new_series = retry_skipped(value['patchwork_url'], skipped_series)
        new_series.extend(get_new_series(value['patchwork_url'],
                                         value['patchwork_project'],
                                         next_date))

        newly_skipped_series = []
        for series in new_series:
            if missing_cover(value['patchwork_url'], series):
                newly_skipped_series.append(series.series_id)

        for series in new_series:
            if series.series_id in newly_skipped_series:
                continue

            trigger = value.copy()
            trigger['skipped_series'] = ' '.join(newly_skipped_series)
            trigger['patchwork'] = ' '.join(series.patches)
            trigger.setdefault('mail_to', trigger.get('mail_to', ''))
            if trigger.get('send_report_to_upstream', False):
                trigger['mail_to'] += ', ' + ', '.join(series.emails)
            # GitLab only allows strings
            trigger['send_report_to_upstream'] = str(
                trigger.get('send_report_to_upstream', False)
            )
            trigger['send_report_on_success'] = str(
                trigger.get('send_report_on_success', False)
            )
            trigger['message_id'] = series.message_id
            trigger['subject'] = 'Re: ' + series.subject
            trigger['date'] = series.last_tested
            trigger['cover_letter'] = series.cover
            trigger['title'] = 'Patch: {}: {}'.format(trigger['name'],
                                                      series.subject)
            triggers.append(trigger)
    return triggers
