"""Pipeline trigger for github repositories."""
import copy
import logging
import hashlib
import subprocess
import os
import json

import github

from . import utils


def generate_hash(github_project_path, pr_id, message_id):
    """
    Generate a sha256 hash used as cki_pipeline_id

    Args:
        github_project_path: Path of the github repo being tested.
        pr_id:               Pull request being tested.
        message_id:          Message id that made the request on the PR.

    Returns:
        A sha256 digest of formatted string of arguments.
    """
    string = f'{github_project_path}@{pr_id}:{message_id}'
    return hashlib.sha256(string.encode('utf-8')).hexdigest()


def is_pr_already_reviewed(pull, comment):
    """
    Check if the pull request in question was already reviewed by the bot.

    Args:
        pull:    PullRequest instance being tested.
        comment: IssueComment instance that made the request on the PR.

    Returns:
        True if PR was already reviewed, otherwise False.
    """
    for review in pull.get_reviews():
        if comment.html_url in review.body:
            return True
    return False


def update_pull_request(pull, comment, cki_status, pipelines_pr):
    """
    Update the pull request, approving or requesting changes based on the
    cki_status passed as parameter.

    Args:
        pull:         PullRequest instance being tested
        comment:      IssueComment instance that made the request on the PR.
        cki_status:   String summarizing the status for all pipelines triggered.
        pipelines_pr: A list of pipelines triggered by the PR.
    """
    if is_pr_already_reviewed(pull, comment):
        return
    if cki_status == 'passed':
        event = 'APPROVE'
        emoji = ':+1::beers:'
    else:
        event = 'REQUEST_CHANGES'
        emoji = ':-1::cry:'
    pipelines_url = '\n'.join([pipeline.web_url for pipeline in pipelines_pr])
    msg = f'{emoji} CKI pipeline {cki_status} {comment.html_url}'
    msg += f'\n\n{pipelines_url}'
    head_commit = pull.head.repo.get_commit(pull.head.sha)
    pull.create_review(head_commit, body=msg, event=event)


def get_variables(project, pipeline):
    """
    Cache the result of utils.get_variables.

    Args:
        project:  Object representing the GitLab project.
        pipeline: Object representing the pipeline to retrieve variables for.

    Returns:
        A dictionary representing job's variables.
    """
    os.makedirs('cache', exist_ok=True)
    path = os.path.join('cache', f'{pipeline.id}.json')
    vars = None
    try:
        with open(path) as file:
            vars = json.load(file)
    except (FileNotFoundError, json.JSONDecodeError):
        """Cache missing or corrupted"""
    if not vars:
        vars = utils.get_variables(project, pipeline)
        with open(path, 'w') as file:
            json.dump(vars, file)
    return vars


def get_last_succesful_baseline(project, cki_pipeline_branch, pages=200):
    """
    Search for the last successful baseline filtering by cki_pipeline_branch.

    Args:
        project:             Gitlab API instance of the cki pipeline project.
        cki_pipeline_branch: CKI pipeline branch.
        pages:               How many past pages of pipelines to check.

    Returns:
        A pipeline instance corresponding to the successful pipeline, otherwise
        None.
    """
    for current_page in range(1, pages + 1):
        logging.info('Fetching page %d/%d', current_page, pages)
        pipelines = project.pipelines.list(page=current_page, per_page=10)

        for pipeline in pipelines:
            if pipeline.status != 'success':
                continue
            vars = get_variables(project, pipeline)
            if vars.get('cki_pipeline_branch') != cki_pipeline_branch:
                continue
            if vars.get('cki_pipeline_type') == 'baseline':
                return pipeline


def filter_pipelines_by_pr(project, pipelines, pull, comment):
    """
    Filter pipelines corresponding to the comment that triggered cki on the PR
    in question.

    Args:
        project:   Gitlab API instance of the cki pipeline project.
        pipelines: A list of pipelines.
        pull:      PullRequest instance being tested.
        comment:   IssueComment instance that made the request on the PR.

    Returns:
        A list of pipelines triggered by the PR.
    """
    pipelines_pr = []
    for pipeline in pipelines:
        vars = get_variables(project, pipeline)
        if vars.get('pr_number') == str(pull.number):
            if vars.get('comment_id') == str(comment.id):
                pipelines_pr.append(pipeline)
    return pipelines_pr


def get_cki_pipeline_status(pull, comment, config, gitlab_instance, pages=200):
    """
    Get the status for every cki pipeline triggered by the PR and summarize it
    in a string.

    Args:
        pull:            PullRequest instance being tested.
        comment:         IssueComment instance that made the request on the PR.
        gitlab_instance: gitlab.Gitlab API instance.
        pages:           How many past pages of pipelines to check.

    Returns:
        A tuple with two values. The first member is a string representing the
        status for all pipelines and where the possible values are "running",
        "failed" and "passed". The second member is a list containing the cki
        pipelines.
    """
    project = gitlab_instance.projects.get(config['cki_project'])
    pipelines_pr = []
    for current_page in range(1, pages + 1):
        logging.info('Fetching page %d/%d', current_page, pages)
        pipelines = project.pipelines.list(page=current_page, per_page=10)
        pipelines_pr = filter_pipelines_by_pr(project, pipelines, pull,
                                              comment)
        if len(pipelines_pr) >= len(config['cki_pipeline_branches']):
            break
    else:
        return (None, None)

    pipeline_running = False
    pipeline_failed = False
    for pipeline in pipelines_pr:
        if pipeline.status == 'running':
            pipeline_running = True
        elif pipeline.status != 'success':
            pipeline_failed = True

    if pipeline_running:
        return ('running', pipelines_pr)
    elif pipeline_failed:
        return ('failed', pipelines_pr)
    return ('passed', pipelines_pr)


def is_comment_already_processed(bot_login, comment):
    """
    Iterate over the comment's reactions to see if the comment was already
    processed.

    Args:
        bot_login: Bot Github's username.
        comment:   IssueComment instance.

    Returns:
        True if bot already put a reaction in the comment, otherwise False.
    """
    for reaction in comment.get_reactions():
        if reaction.user.login == bot_login:
            return True
    return False


def check_pipelines_for_update_pr(pull, comment, config, gitlab_instance):
    """
    Check the status for all pipelines and update the PR if they are finished.

    Args:
        pull:            PullRequest instance being tested.
        comment:         IssueComment instance that made the request on the PR.
        config:          Dictionary with the trigger configuration.
        gitlab_instance: gitlab.Gitlab API instance.
    """
    if is_pr_already_reviewed(pull, comment):
        return
    status, pipelines_pr = get_cki_pipeline_status(pull, comment, config,
                                                   gitlab_instance)
    logging.info('CKI pipeline status for PR %d %r', pull.number, status)
    if status and status != 'running':
        update_pull_request(pull, comment, status, pipelines_pr)


# TODO (TiN): Add support for setting variables (e.g. choosing a different
# kpet-db branch)
def process_comment(bot_login, repo, pull, comment, config, gitlab_instance):
    """
    Process a comment looking for the word 'test'. If 'test' is present, it
    builds new triggers for the CKI pipeline.

    Args:
        bot_login:       Bot Github's username.
        repo:            Repository instance being tested.
        pull:            PullRequest instance being tested.
        comment:         IssueComment instance that made the request on the PR.
        config:          Dictionary with the trigger configuration.
        gitlab_instance: gitlab.Gitlab API instance.

    Returns:
        A list of triggers if the comment is processed, otherwise None.
    """
    if is_comment_already_processed(bot_login, comment):
        return
    if 'test' not in comment.body:
        return
    triggers = []
    logging.info('processing %s', comment.body)
    project = gitlab_instance.projects.get(config['cki_project'])
    for branch in config['cki_pipeline_branches']:
        pipeline = get_last_succesful_baseline(project, branch)
        variables = get_variables(project, pipeline)
        variables['cki_project'] = config['cki_project']
        variables['pr_number'] = str(pull.number)
        variables['comment_id'] = str(comment.id)
        variables['github_project'] = config['github_project_path']
        var = config['variable_to_replace']
        fmt = config['format_to_replace']
        variables[var] = fmt.format(**locals())
        variables['mail_to'] = pull.user.email or config['mail_to']
        variables['require_manual_review'] = 'False'
        variables['cki_pipeline_type'] = f'{repo.name}@{pull.head.ref}'
        variables['title'] = 'Retrigger {}: {}'.format(
            variables['cki_pipeline_type'],
            variables['title'],
        )
        variables['cki_pipeline_id'] = generate_hash(
            config['github_project_path'], pull.id, comment.id
        )
        variables.update(config.get('variables', {}))
        if variables.get('skip_build') == 'true':
            base_url = f'{project.web_url}/-/jobs/{{}}/artifacts/download'
            for job in pipeline.jobs.list():
                if job.stage == 'build':
                    arch = job.name.split()[1]
                    variables[f'ARTIFACT_URL_{arch}'] = base_url.format(job.id)
        logging.info('New trigger with variables %r', variables)
        triggers.append(variables)
    comment.create_reaction('+1')
    pull.create_issue_comment("CKI pipeline triggered")
    return triggers


def load_triggers(gitlab_instance, cki_github_config):
    """
    Get ready all potential triggers.
    """
    triggers = []
    github_private_token = utils.get_env_var_or_raise('GITHUB_PRIVATE_TOKEN')
    ghub = github.Github(github_private_token)
    bot_login = ghub.get_user().login
    for key, value in cki_github_config.items():
        repo = ghub.get_repo(value['github_project_path'])
        for pull in repo.get_pulls():
            logging.info('%r', pull)
            for comment in pull.get_issue_comments():
                if f'@{bot_login}' in comment.body:
                    pull_request_triggers = process_comment(
                        bot_login, repo, pull, comment, value, gitlab_instance
                    )
                    if pull_request_triggers:
                        triggers.extend(pull_request_triggers)
                    else:
                        check_pipelines_for_update_pr(pull, comment, value,
                                                      gitlab_instance)
    return triggers
