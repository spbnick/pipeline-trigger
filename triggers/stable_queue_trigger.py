"""
Pipeline trigger for stable queues.

1. Get the current stable queue for the requested version. The patches to be
   applied in the current version are in the queue-<version>/series file, which
   gets parsed.
2. Check if a pipeline with currently retrieved patches already ran.
3. Profit $$$.
"""
import hashlib
import io
import logging
import os
import subprocess

from . import utils


def generate_hash(branch, baseline, patches):
    """
    Generate a hash used as cki_pipeline_id.

    Args:
        branch:   Git branch being tested.
        baseline: Commit used as the test basline.
        patches:  List of patch filenames applied to the baseline.

    Returns:
        sha256 digest of the formatted arguments.
    """
    string = f'{branch}@{baseline}:{patches}'
    return hashlib.sha256(string.encode('utf-8')).hexdigest()


def get_patches(repo_url, directory):
    """
    Get a list of patch URLs and patch filenames for given stable queue repo
    and directory associated with a release.

    We can't pass files to the pipeline from the trigger but have to download
    them later, so we need the list of URLs. For the skt invocation itself
    though, we need the actual filenames. No need to parse them out later when
    we can just get them here.

    Args:
        repo_url:  URL of the stable queue git repository.
        directory: Directory in the stable queue repository associated with
                   tested kernel release.

    Returns:
        A tuple ([list_of_patch_urls], [list_of_patch_filenames])
    """
    patch_urls = []
    patch_filenames = []

    # Since the 'series' file can contain comments (and eg. commented out
    # patches), get the actual list of patches to apply by parsing
    # 'quilt series' output. For this, we need to download the file first.
    series_url = f'{repo_url}/plain/{directory}/series'
    try:
        subprocess.check_call(['wget', series_url])
    except subprocess.CalledProcessError:
        # The series file or the whole directory is not present, so there are
        # no patches in the queue for this release. Or the whole web with the
        # git tree is down. We don't care since a) we can't test anything
        # anyways, and b) we will be calling this trigger often enough that we
        # don't need a special code to handle this situation.
        logging.info('No series file for %s available', directory)
        return ([], [])

    quilt = subprocess.Popen(
        ['quilt', 'series'],
        stdout=subprocess.PIPE,
        env=dict(os.environ,
                 **{'QUILT_PATCHES': f'{repo_url}/plain/{directory}'})
    )
    for line in io.TextIOWrapper(quilt.stdout, encoding='utf-8'):
        line = line.strip()
        patch_urls.append(line)
        patch_filenames.append(os.path.basename(line))

    return (patch_urls, patch_filenames)


def load_triggers(gitlab_instance, stable_config):
    triggers = []

    # Translate the human-friendly yaml terminology to skt and remove nesting,
    # just so we don't have to deal with it later when triggering the pipeline.
    stable_config['baserepo'] = stable_config['git_url']
    del stable_config['git_url']
    stable_config['cki_pipeline_type'] = 'stable_queue'
    # Don't send internal Beaker URLs to upstream lists
    stable_config['report_template'] = 'limited'

    for stable_version in stable_config['test']:
        for _, stable_data in stable_version.items():
            trigger = stable_config.copy()
            project = gitlab_instance.projects.get(trigger['cki_project'])

            trigger['branch'] = stable_data['git_branch']
            trigger['queue_dir'] = stable_data['queue_dir']
            del trigger['test']

            trigger['ref'] = utils.get_commit_hash(trigger['baserepo'],
                                                   trigger['branch'])
            patch_urls, localpatches = get_patches(trigger['queue_url'],
                                                   trigger['queue_dir'])
            trigger['patch_urls'] = ' '.join(patch_urls)
            trigger['localpatches'] = ' '.join(localpatches)

            if not trigger['localpatches']:  # No patches to test
                continue

            trigger['cki_pipeline_id'] = generate_hash(trigger['branch'],
                                                       trigger['ref'],
                                                       trigger['localpatches'])
            trigger['send_report_on_success'] = str(
                trigger.get('send_report_on_success', False)
            )

            if not trigger.get('make_target'):
                trigger['make_target'] = 'targz-pkg'

            do_not_trigger = utils.was_tested(project,
                                              trigger['cki_pipeline_branch'],
                                              trigger['cki_pipeline_id'])
            if do_not_trigger:
                logging.info('Pipeline for %s queue already triggered.',
                             trigger['branch'])
            else:
                trigger['title'] = 'Stable queue: {}'.format(
                    trigger['queue_dir']
                )
                trigger['subject'] = trigger['title']
                triggers.append(trigger)

    return triggers
