"""Common main() for all triggers."""
import argparse
import logging
import sys
from yaml import load, Loader

import gitlab

from . import TRIGGERS
from .utils import get_env_var_or_raise, trigger_pipelines


def main():
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG,
                        stream=sys.stdout)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("github").setLevel(logging.WARNING)

    parser = argparse.ArgumentParser(description='Pipeline triggers')
    parser.add_argument(
        '-c', '--config',
        type=str,
        help=(
            'YAML configuration file to use. Will try to use `<name>.yaml` '
            'from the current working directory if not specified, where '
            '`<name>` is one of: {}'.format(list(TRIGGERS.keys()))
        )
    )
    parser.add_argument(
        '--token',
        type=str,
        default='GITLAB_TRIGGER_TOKEN',
        help=(
            'Name of the env var which contains GitLab trigger token. Defaults'
            ' to `GITLAB_TRIGGER_TOKEN`.'
        )
    )
    parser.add_argument('trigger',
                        choices=TRIGGERS.keys(),
                        help=('Name of the pipeline trigger'))
    args = parser.parse_args()

    gitlab_url = get_env_var_or_raise('GITLAB_URL')
    private_token = get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
    trigger_token = get_env_var_or_raise(args.token)
    configfile = args.config if args.config else '{}.yaml'.format(args.trigger)

    with open(configfile) as config_file:
        config = load(config_file, Loader=Loader)

    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=4)

    loader = TRIGGERS[args.trigger].load_triggers
    triggers = loader(gitlab_instance, config)
    trigger_pipelines(gitlab_instance, trigger_token, triggers)


if __name__ == '__main__':
    main()
