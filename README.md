Writing new triggers - tl;dr
----------------------------

* By default, a `<trigger-name>.yaml` file is expected to contain the trigger
  configuration. For testing, you can create any file you want and pass it over
  with the `--config <filename>` option.
* Create a `<trigger-name>_trigger.py` file in the `triggers/` directory. This
  file contains the code needed for transformation of the configuration and any
  external data into the variables passed to the actual pipeline trigger.
* Add a new trigger key to the mapping in `triggers/__init__.py`.
* Write tests for your code !!!


Some more details, hints and suggestions
----------------------------------------

* If possible, use the functions in `triggers/utils.py` instead of writing new
  ones for the same functionality. It makes it easier to test and maintain.
* The entry point of the `<trigger-name>_trigger.py` should be a
  `load_triggers` function that takes two arguments, which are passed by the
  `main()` function:
  * `gitlab_instance` -- an `gitlab.Gitlab` object
  * `config` -- parsed YAML configuration
* `load_triggers()` should return a list of dictionaries, each dictionary fully
  describing the pipeline to trigger. The mandatory parameters are:
  * `cki_project`: The project to trigger the pipelines in, in the
                   `username/repo` format. Should be read from the
                   configuration.
  * `cki_pipeline_branch`: Branch of the `cki_project` which should be used for
                           reference commit creation and pipeline triggering.
                           Should also be read from the configuration.
  * `title`: Title that should be used to identify the commit. Should be created
             based on what's tested.
  * `cki_pipeline_type`: A string describing the type of the test, eg.
    `baseline` or `patchwork`.
* Some optional parameters for the pipeline description include:
  * Any variables the pipeline needs for functioning.
  * `cki_pipeline_id`: A unique hash identifying the pipeline run.
