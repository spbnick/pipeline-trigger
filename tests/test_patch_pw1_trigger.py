"""Tests for triggers.patch_pw1_trigger."""
import datetime
import os
import unittest
import xmlrpc.client
import yaml

import mock

from triggers.utils import SeriesData
import triggers.patch_pw1_trigger as pw1_trigger

import fakes


class TestRPCWrapper(unittest.TestCase):
    """Tests for pw1_trigger.RPCWrapper class."""
    def test_wrapper_functionality(self):
        """Verify RPCWrapper correctly wraps PW1 interface."""
        to_wrap = fakes.WrapMe()
        wrapped = pw1_trigger.RPCWrapper(to_wrap)

        returned_value = wrapped.test_me()
        # No versions returned
        self.assertEqual(returned_value, 'value')


class TestAuthTransport(unittest.TestCase):
    """Tests for pw1_trigger.BasicHTTPAuthTransport class."""
    def test_send_header(self):
        """
        Verify the auth header is set with the data we want and we are not
        ignoring the original headers.
        """
        user = 'user'
        password = 'password'
        expected_header = b'Basic dXNlcjpwYXNzd29yZA=='
        fake_connection = fakes.FakeConnection()

        transport = pw1_trigger.BasicHTTPAuthTransport(user, password)
        transport.send_headers(fake_connection,
                               (('key1', 'val1'), ('key2', 'val2')))

        headers = fake_connection.get_headers()
        self.assertEqual(expected_header, headers['Authorization'])
        self.assertEqual('val1', headers['key1'])
        self.assertEqual('val2', headers['key2'])
        # Verify we don't pass any more custom headers
        self.assertEqual(3, len(headers))


class TestCookieRetrieval(unittest.TestCase):
    """Tests for pw1_trigger.get_cookie()."""
    @mock.patch('requests.session')
    def test_cookie_calls(self, mock_session):
        session_instance = mock_session.return_value
        patchwork_url = 'http://patchwork.url'
        user = 'user_1'
        password = 'secret_password_123'

        session_instance.cookies.get_dict.return_value = {
            'sessionid': 'cookie'
        }

        self.assertEqual(pw1_trigger.get_cookie(user, password, patchwork_url),
                         'cookie')
        session_instance.__enter__.return_value.post.assert_called_with(
            patchwork_url + '/accounts/login/',
            data=f'username={user}&password={password}'
        )


class TestGetLastPatchRetrieval(unittest.TestCase):
    """Tests for pw1_trigger.get_last_patch()."""
    @mock.patch('triggers.utils.get_variables')
    def test_get_last_patch(self, mock_variables):
        """Verify get_last_patch retrieves last tested patch."""
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        last_patch_id = 123456
        mock_variables.side_effect = [
            # Don't use last_patch_id here to see if it gets skipped correctly
            {'cki_pipeline_type': 'patchwork-v1',
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'},
            {'cki_pipeline_type': 'patchwork-v1',
             'last_patch_id': last_patch_id,
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'}
        ]

        project = fakes.FakeGitLabProject()
        # Create some fun pipelines
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='failed'
        )
        project.trigger_pipeline(
            'cki_branch2', 'token', {'ref': 'cki_branch2'}, status='pending'
        )
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='success'
        )

        self.assertEqual(pw1_trigger.get_last_patch(project, data),
                         last_patch_id)

    def test_no_patch(self):
        """
        Verify get_last_patch raises Exception if there is no date to retrieve.
        """
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline(
            'cki_branch2', 'token', {'ref': 'cki_branch2'}, status='pending'
        )

        with self.assertRaises(Exception):
            pw1_trigger.get_last_patch(project, data)

    @mock.patch('triggers.utils.get_variables')
    def test_differet_project(self, mock_variables):
        """
        Verify get_last_patch returns patch for the correct source if more
        Patchwork instances / projects are used with the same branch.
        """
        last_patch_id = 123456
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        mock_variables.side_effect = [
            {'cki_pipeline_type': 'patchwork-v1',
             'patchwork_url': 'patchwork_2',
             'last_patch_id': 'not this one',
             'patchwork_project': 'pw_project'},
            {'cki_pipeline_type': 'patchwork-v1',
             'last_patch_id': last_patch_id,
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'}
        ]

        project = fakes.FakeGitLabProject()
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='failed'
        )
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='success'
        )

        self.assertEqual(pw1_trigger.get_last_patch(project, data),
                         last_patch_id)


class TestGetNewSeries(unittest.TestCase):
    """Tests for pw1_trigger.get_new_series()."""
    @mock.patch('triggers.patch_pw1_trigger.get_series_by_patch')
    def test_correct_call(self, mock_series_by_patch):
        """
        Verify the function calls get_series_by_patch with right arguments.
        """
        patchwork_url = 'http://patchwork.test'
        project_name = 'myproject'
        rpc = fakes.FakeServerProxy()
        rpc.add_project({'id': 1, 'linkname': project_name})

        pw1_trigger.get_new_series(rpc, project_name, 123, patchwork_url)
        self.assertEqual(mock_series_by_patch.call_args[0],
                         ([], patchwork_url))

    @mock.patch('triggers.patch_pw1_trigger.restore_patch_name')
    def test_project_exception(self, mock_series_by_patch):
        """
        Verify the function raises Exception if the expected project doesn't
        exist.
        """
        patchwork_url = 'http://patchwork.test'
        rpc = fakes.FakeServerProxy()
        rpc.add_project({'id': 1, 'linkname': 'notmyproject'})

        with self.assertRaises(Exception):
            pw1_trigger.get_new_series(rpc, 'name', 123, patchwork_url)

    @mock.patch('triggers.patch_pw1_trigger.get_series_by_patch')
    @mock.patch('triggers.patch_pw1_trigger.restore_patch_name')
    def test_patch_name_change(self, mock_renames, mock_series_by_patch):
        """Verify the patch renaming function is called."""
        patchwork_url = 'http://patchwork.test'
        project_name = 'myproject'
        rpc = fakes.FakeServerProxy()
        rpc.add_project({'id': 1, 'linkname': project_name})
        rpc.add_patch({'id': 1, 'project_id': 1})

        pw1_trigger.get_new_series(rpc, project_name, 0, patchwork_url)
        mock_renames.assert_called_once()


class TestPatchRename(unittest.TestCase):
    """Tests for pw1_trigger.restore_patch_name()."""
    def test_rename(self):
        headers = 'From: xxx xxx@yyy\nSubject: real-subject'
        patch = {'name': 'fake-name', 'root_comment': {'headers': headers}}
        pw1_trigger.restore_patch_name(patch)
        self.assertEqual(patch['name'], 'real-subject')


class TestSeriesByPatch(unittest.TestCase):
    """Tests for pw1_trigger.get_series_by_patch()."""
    def test_get_series_by_patch(self):
        """
        Verify the functionality of get_series_by_patch(). Check single patches
        and series are parsed correctly and patches with SKIP patterns in name
        are skipped.
        """
        patchwork_url = 'http://patchwork.test'
        patch_data = [
            # A patch to be dropped
            {'name': '[PATCH iproute] dont-submit'},
            # Series with a cover
            {'name': '[PATCH 0/2] something',
             'msgid': '<123.456-1-surname@email.org>',
             'id': 1,
             'root_comment': {'headers': ''}},
            {'name': '[PATCH 1/2] something',
             'msgid': '<123.456-2-surname@email.org>',
             'id': 2,
             'root_comment': {'headers': 'To: name@email'}},
            {'name': '[PATCH 2/2] something',
             'msgid': '<123.456-3-surname@email.org>',
             'id': 3,
             'root_comment': {'headers': ''}},
            # A single patch
            {'name': '[PATCH] something else',
             'msgid': '<124.567-1-surname@email.org>',
             'id': 4,
             'root_comment': {'headers': 'From: name2@email'}},
            # Something got crazy and the series have no useful messge IDs
            {'name': '[PATCH 1/2] magic',
             'msgid': 'stuff',
             'submitter_id': 111,
             'id': 5,
             'root_comment': {'headers': ''}},
            {'name': '[PATCH 2/2] magic',
             'msgid': 'stuff2',
             'submitter_id': 111,
             'id': 6,
             'root_comment': {'headers': ''}}]
        expected_data = [
            SeriesData(subject='[PATCH 2/2] something',
                       message_id='<123.456-3-surname@email.org>',
                       last_tested=str(3),
                       cover=patchwork_url + '/patch/1/mbox',
                       patches=[patchwork_url + '/patch/2',
                                patchwork_url + '/patch/3'],
                       emails=set(['name@email'])),
            SeriesData(subject='[PATCH] something else',
                       message_id='<124.567-1-surname@email.org>',
                       last_tested=str(4),
                       patches=[patchwork_url + '/patch/4'],
                       emails=set(['name2@email'])),
            SeriesData(subject='[PATCH 2/2] magic',
                       message_id='stuff2',
                       last_tested=str(6),
                       patches=[patchwork_url + '/patch/5',
                                patchwork_url + '/patch/6'],
                       emails=set())
        ]

        returned_data = pw1_trigger.get_series_by_patch(patch_data,
                                                        patchwork_url)
        self.assertEqual(returned_data, expected_data)

    def test_get_series_by_patch_exception(self):
        """
        Verify get_series_by_patch raises an exception if it gets bad response
        from Patchwork.
        """
        patch_api_url = 'http://patchwork.test/api/patches'

        with self.assertRaises(Exception):
            pw1_trigger.get_series_by_patch('patchwork', patch_api_url)


class TestSanitizeEmails(unittest.TestCase):
    """Tests for pw1_trigger.sanitize_emails()."""
    def test_no_patches(self):
        """Verify sanitize_emails works even if there are no patches."""
        self.assertEqual(set(), pw1_trigger.sanitize_emails([]))

    def test_no_headers(self):
        """Verify sanitize_emails works even if there are no target headers."""
        headers = 'HeaderX: xxx\nDate: invalid\n'
        patch = {'root_comment': {'headers': headers}}
        self.assertEqual(set(), pw1_trigger.sanitize_emails([patch]))

    def test_with_data(self):
        headers = '{}\n{}\n{}'.format(
            'HeaderX: invalid',
            'From: Name Surname <name@email>',
            'To: Alias <name@email>, \n someoneelse@email'
        )
        patch = {'root_comment': {'headers': headers}}
        self.assertEqual(set(['name@email', 'someoneelse@email']),
                         pw1_trigger.sanitize_emails([patch]))


class TestLoadTriggers(unittest.TestCase):
    """Tests for pw1_trigger.load_triggers()."""
    @mock.patch('triggers.patch_pw1_trigger.get_cookie')
    @mock.patch('triggers.patch_pw1_trigger.RPCWrapper')
    @mock.patch('xmlrpc.client.ServerProxy')
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.patch_pw1_trigger.get_last_patch')
    @mock.patch('triggers.patch_pw1_trigger.get_new_series')
    def test_set_values(self, mock_series, mock_last_patch, mock_commit,
                        mock_xmlrpc, mock_wrapper, mock_cookie):
        """
        Verify all required keys and values are set and renamed if the config
        is valid.
        """
        os.environ.update({'PATCHWORK_USER': 'user',
                           'PATCHWORK_PASSWORD': 'password'})

        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branch: master\n'
            '  cki_project: username/project\n'
            '  baseline_project: username/baseline_project\n'
            '  cki_pipeline_branch: test_name\n'
            '  patchwork:\n'
            '    url: http://patchwork.url\n'
            '    project: patchwork-project\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')
        gitlab.add_project('username/baseline_project')

        mock_commit.return_value = 'baseline_ref'
        mock_last_patch.return_value = 1234
        mock_series.return_value = [SeriesData(
            patches=['http://patchwork.url/patch/123'],
            emails=set(['name@email']),
            subject='subject',
            message_id='message_id',
            last_tested=str(1235)
        )]
        mock_wrapper.return_value = fakes.FakeServerProxy()
        mock_cookie.return_value = 'abcdef0123456789'

        expected_trigger = {'baserepo': 'http://git.test/git/kernel.git',
                            'patchwork_url': 'http://patchwork.url',
                            'patchwork_project': 'patchwork-project',
                            'cki_pipeline_type': 'patchwork-v1',
                            'name': 'test_name',
                            'ref': 'baseline_ref',
                            'branch': 'master',
                            'cki_pipeline_branch': 'test_name',
                            'cki_project': 'username/project',
                            'baseline_project': 'username/baseline_project',
                            'make_target': 'targz-pkg',
                            'patchwork': 'http://patchwork.url/patch/123',
                            'subject': 'Re: subject',
                            'message_id': 'message_id',
                            'send_report_to_upstream': 'False',
                            'send_report_on_success': 'False',
                            'last_patch_id': '1235',
                            'cover_letter': '',
                            'patchwork_session_cookie': 'abcdef0123456789',
                            'title': 'Patch: test_name: subject'}

        trigger = pw1_trigger.load_triggers(gitlab, config)[0]
        self.maxDiff = None
        self.assertDictEqual(trigger, expected_trigger)
        # Verify baseline retrieval is called with the baseline project, not
        # cki one.
        mock_commit.assert_called_with(config['test_name']['git_url'],
                                       config['test_name']['branch'])

        config_text += '  send_report_to_upstream: True\n'
        config = yaml.safe_load(config_text)
        trigger = pw1_trigger.load_triggers(gitlab, config)[0]
        expected_trigger['send_report_to_upstream'] = "True"
        expected_trigger['mail_to'] = 'name@email'
        self.assertDictEqual(trigger, expected_trigger)

        # Verify emails aren't configurable. NO LEAKS!!!
        config_text += '  mail_to: foo@bar.com\n'
        config = yaml.safe_load(config_text)
        expected_trigger['mail_to'] = 'name@email'
        trigger = pw1_trigger.load_triggers(gitlab, config)[0]
        self.assertDictEqual(trigger, expected_trigger)

        del os.environ['PATCHWORK_USER']
        del os.environ['PATCHWORK_PASSWORD']
