"""Tests for triggers.utils."""
import json
import os
import unittest

import mock
import responses

import triggers.utils as utils

import fakes


class TestGetEnvVarOrRaise(unittest.TestCase):
    """Test cases for utils.get_env_var_or_raise()."""
    def test_existing_variable(self):
        """Test the function retrieves the value of existing variable."""
        os.environ.update({'TEST_EXISTING': 'test_value'})
        self.assertEqual(utils.get_env_var_or_raise('TEST_EXISTING'),
                         'test_value')
        del os.environ['TEST_EXISTING']

    def test_nonexistent_variable(self):
        """
        Test the function raises EnvVarNotSetError if the variable doesn't
        exist.
        """
        if os.environ.get('TEST_NONEXISTENT'):
            del os.environ['TEST_NONEXISTENT']

        with self.assertRaises(utils.EnvVarNotSetError):
            utils.get_env_var_or_raise('TEST_NONEXISTENT')


class TestVariableRetrieval(unittest.TestCase):
    """Test cases for utils.get_variables()."""
    @responses.activate
    def test_job_has_variables(self):
        """Test the function returns all existing variables of the job."""
        variables = {'key1': 'value1', 'key2': 'value2'}

        trigger_vars = []
        for key, value in variables.items():
            var = {
                'key': key,
                'value': value,
            }
            trigger_vars.append(var)
        fake_data = {
            'trigger': {
                'variables': trigger_vars,
            },
        }

        project = fakes.FakeGitLabProject()
        pipeline = fakes.FakePipeline('foo', 'bar', variables, 'baz')
        responses.add(responses.GET,
                      'http://web-url/-/jobs/1.json',
                      status=200,
                      body=json.dumps(fake_data))

        self.assertEqual(utils.get_variables(project, pipeline), variables)

    @responses.activate
    def test_no_variables(self):
        """
        Verify get_variables catches a KeyError when there are no variables.
        """
        responses.add(responses.GET,
                      'http://web-url/-/jobs/1.json',
                      status=200,
                      body=json.dumps({}))
        project = fakes.FakeGitLabProject()
        pipeline = fakes.FakePipeline('foo', 'bar', {}, 'baz')

        self.assertEqual(utils.get_variables(project, pipeline), {})

    @responses.activate
    def test_job_without_variables(self):
        """Test the function works even if there are no variables."""
        fake_data = {
            'trigger': {
                'variables': {},
            },
        }

        responses.add(responses.GET,
                      'http://web-url/-/jobs/1.json',
                      status=200,
                      body=json.dumps(fake_data))

        project = fakes.FakeGitLabProject()
        pipeline = fakes.FakePipeline('foo', 'bar', {}, 'baz')
        self.assertEqual(utils.get_variables(project, pipeline), {})

    def test_pipeline_without_jobs(self):
        """
        Test the function returns {} if there aren't any jobs associated with
        the pipeline.
        """
        project = fakes.FakeGitLabProject()
        pipeline = fakes.FakePipeline('foo', 'bar', {}, 'baz')
        # Overwrite the job entry created by default
        pipeline.jobs._jobs = []  # pylint: disable=W0212

        self.assertEqual(utils.get_variables(project, pipeline), {})

    @responses.activate
    def test_job_unexpected_response(self):
        """Test the function raises exception if it can't reach GitLab."""
        responses.add(responses.GET,
                      'http://web-url/-/jobs/1.json',
                      status=500)

        project = fakes.FakeGitLabProject()
        pipeline = fakes.FakePipeline('foo', 'bar', {}, 'baz')

        with self.assertRaises(Exception):
            utils.get_variables(project, pipeline)


class TestCreateCommit(unittest.TestCase):
    """Test cases for utils.create_commit()."""
    def test_created_data(self):
        """Verify the content of created commit looks as expected."""

        commit_text = 'title\n\ncki_pipeline_branch = branch'
        expected_data = {'branch': 'branch',
                         'actions': [],
                         'commit_message': commit_text}

        project = fakes.FakeGitLabProject()
        utils.create_commit(project,
                            {'cki_pipeline_branch': 'branch'},
                            'title')

        self.assertEqual(project.commits[0], expected_data)


class TestCheckForTested(unittest.TestCase):
    """Tests for utils.was_tested()."""
    @mock.patch('triggers.utils.get_variables')
    def test_was_tested_true(self, mock_variables):
        """Check was_tested returns True if the pipeline already ran."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_2', 'token', {'ref': 'cki_2'})

        mock_variables.return_value = {'cki_pipeline_id': 'id',
                                       'ref': 'sha',
                                       'branch': 'git_branch'}

        self.assertTrue(utils.was_tested(project, 'cki_2', 'id'))
        mock_variables.assert_called_once()

    @mock.patch('triggers.utils.get_variables')
    def test_was_tested_false(self, mock_variables):
        """Check was_tested returns False if the pipeline didn't run."""
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})
        project.trigger_pipeline('cki_branch', 'token', {'ref': 'cki_branch'})

        mock_variables.return_value = {'cki_pipeline_id': 'different_id',
                                       'ref': 'sha',
                                       'branch': 'git_branch'}

        self.assertFalse(utils.was_tested(
            project, 'cki_branch', 'id', pages=1
        ))
        self.assertEqual(mock_variables.call_count, 2)


class TestTrigger(unittest.TestCase):
    """Tests for utils.trigger_pipelines()."""
    @mock.patch('logging.info')
    def test_trigger_single(self, mock_logging):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        variables = {'cki_project': 'cki-project',
                     'cki_pipeline_branch': 'test_branch',
                     'title': 'commit_title'}

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        utils.trigger_pipelines(gitlab, 'token', [variables])

        pipeline = gitlab.projects['cki-project'].pipelines[0]
        for key, value in variables.items():
            self.assertEqual(pipeline.attributes[key], value)

        mock_logging.assert_called_once()

    def test_required_variables(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        variables = {'cki_project': 'cki-project',
                     'cki_pipeline_branch': 'test_branch',
                     'title': 'commit_title'}

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('cki-project')

        for expected in variables:
            missing = variables.copy()
            del missing[expected]
            with self.assertRaises(KeyError):
                utils.trigger_pipelines(gitlab, 'token', [missing])


class TestGetCommitHash(unittest.TestCase):
    """Tests for utils.get_commit_hash()."""
    @mock.patch('subprocess.check_output')
    def test_commit_hash(self, mock_check_output):
        """Verify get_commit_hash returns the right value from git output."""
        mock_check_output.return_value = 'hash\t\trefs/master'.encode()
        self.assertEqual(utils.get_commit_hash('repo', 'branch'), 'hash')
