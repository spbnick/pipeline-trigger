"""Tests for triggers.baseline_trigger."""
import hashlib
import unittest
import yaml

import mock

import triggers.baseline_trigger as baseline

import fakes


class TestHash(unittest.TestCase):
    """Tests for baseline.generate_hash()."""
    def test_hash_all(self):
        """
        Verify generate_hash() returns expected hash if all arguments are
        nonempty. Only make sure the function is not changed without people
        noticing since it can break checks of already tested queues!
        """
        git_repo, branch, commit = 'git_repo', 'branch', 'abcde'
        data = '{}@{}:{}'.format(git_repo, branch, commit)
        self.assertEqual(hashlib.sha256(data.encode('utf-8')).hexdigest(),
                         baseline.generate_hash(git_repo, branch, commit))

    def test_nones(self):
        """Verify generate_hash() works if any arguments are None."""
        data = '{}@{}:{}'.format(None, None, None)
        self.assertEqual(hashlib.sha256(data.encode('utf-8')).hexdigest(),
                         baseline.generate_hash(None, None, None))


class TestLoadTriggers(unittest.TestCase):
    """Tests for baseline.load_triggers()."""
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.baseline_trigger.generate_hash')
    @mock.patch('triggers.utils.was_tested')
    def test_set_values(self, mock_tested, mock_hash, mock_commit):
        """
        Verify all required keys and values are set and renamed if the config
        is valid.
        """
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branches:\n'
            '    - master\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tested.return_value = False
        mock_commit.return_value = 'current_baseline'
        mock_hash.return_value = 'new_hash'

        expected_trigger = {
            'baserepo': 'http://git.test/git/kernel.git',
            'cki_pipeline_type': 'baseline',
            'name': 'test_name',
            'ref': 'current_baseline',
            'cki_pipeline_id': 'new_hash',
            'branch': 'master',
            'make_target': 'targz-pkg',
            'cki_pipeline_branch': 'test_name',
            'cki_project': 'username/project',
            'title': 'Baseline: test_name master@current_baseline',
            'require_manual_review': 'True',
        }

        trigger = baseline.load_triggers(gitlab, config)[0]
        self.assertEqual(trigger, expected_trigger)

    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.baseline_trigger.generate_hash')
    @mock.patch('triggers.utils.was_tested')
    @mock.patch('logging.info')
    def test_already_triggered(self, mock_logging, mock_tested, mock_hash,
                               mock_commit):
        """Verify already triggered pipeline is not triggered again."""
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branches:\n'
            '    - master\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_tested.return_value = True
        mock_commit.return_value = 'current_baseline'
        mock_hash.return_value = 'new_hash'

        triggers = baseline.load_triggers(gitlab, config)
        self.assertEqual(triggers, [])
        mock_logging.assert_called_once()
