"""Tests for triggers.patch_trigger."""
import datetime
import unittest
from urllib.parse import quote
import yaml

import dateutil.parser
import mock
import responses

from triggers.utils import SeriesData
import triggers.patch_trigger as patch_trigger

import fakes


class TestNextDateAndSkippedSeriesRetrieval(unittest.TestCase):
    """Tests for patch_trigger.get_next_date_and_skipped_series()."""
    @mock.patch('triggers.utils.get_variables')
    def test_get_next_date(self, mock_variables):
        """
        Verify get_next_date_and_skipped_series retrieves newest variables.
        """
        old_date = '2018-07-19T00:45:18'
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        mock_variables.side_effect = [
            # Don't use date here to see if it gets skipped correctly
            {'cki_pipeline_type': 'patchwork',
             'patchwork_url': 'patchwork',
             'skipped_series': '',
             'patchwork_project': 'pw_project'},
            {'cki_pipeline_type': 'patchwork',
             'date': old_date,
             'skipped_series': '',
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'}
        ]

        project = fakes.FakeGitLabProject()
        # Create some fun pipelines
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='failed'
        )
        project.trigger_pipeline(
            'cki_branch2', 'token', {'ref': 'cki_branch2'}, status='pending'
        )
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='success'
        )

        expected_date = dateutil.parser.parse(
            old_date) + datetime.timedelta(seconds=1)
        self.assertEqual(
            patch_trigger.get_next_date_and_skipped_series(project, data),
            (expected_date, [])
        )

    def test_no_date(self):
        """
        Verify get_next_date_and_skipped_series raises Exception if there is no
        date to retrieve.
        """
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        project = fakes.FakeGitLabProject()
        project.trigger_pipeline(
            'cki_branch2', 'token', {'ref': 'cki_branch2'}, status='pending'
        )

        with self.assertRaises(Exception):
            patch_trigger.get_next_date_and_skipped_series(project, data)

    @mock.patch('triggers.utils.get_variables')
    def test_differet_project(self, mock_variables):
        """
        Verify get_next_date_and_skipped_series returns data for the correct
        source if more Patchwork instances / projects are used with the same
        branch.
        """
        old_date = '2018-07-19T00:45:18'
        data = {'cki_pipeline_branch': 'cki_branch',
                'patchwork_url': 'patchwork',
                'patchwork_project': 'pw_project'}
        mock_variables.side_effect = [
            # Don't use date here to see if it gets skipped correctly
            {'cki_pipeline_type': 'patchwork',
             'patchwork_url': 'patchwork_2',
             'date': 'non-parsable',
             'skipped_series': '',
             'patchwork_project': 'pw_project'},
            {'cki_pipeline_type': 'patchwork',
             'date': old_date,
             'skipped_series': '',
             'patchwork_url': 'patchwork',
             'patchwork_project': 'pw_project'}
        ]

        project = fakes.FakeGitLabProject()
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='failed'
        )
        project.trigger_pipeline(
            'cki_branch', 'token', {'ref': 'cki_branch'}, status='success'
        )

        expected_date = dateutil.parser.parse(
            old_date) + datetime.timedelta(seconds=1)
        self.assertEqual(
            patch_trigger.get_next_date_and_skipped_series(project, data),
            (expected_date, [])
        )


class TestGetNewSeries(unittest.TestCase):
    """Tests for patch_trigger.get_new_series()."""
    @mock.patch('triggers.patch_trigger.get_series_by_patch')
    def test_correct_call(self, mock_series_by_patch):
        """
        Verify the function calls get_series_by_patch with right arguments.
        """
        patchwork_url = 'http://patchwork.test'
        patchwork_project = 'myproject'
        date = dateutil.parser.parse('2018-07-19T00:45:18')
        expected_endpoint = '{}/api/patches?project={}&since={}'.format(
            patchwork_url, patchwork_project, quote(date.isoformat())
        )

        patch_trigger.get_new_series(patchwork_url, patchwork_project, date)
        self.assertEqual(mock_series_by_patch.call_args[0],
                         (patchwork_url, expected_endpoint))


class TestMissingCover(unittest.TestCase):
    """Tests for patch_trigger.missing_cover()."""
    @responses.activate
    def test_missing_cover_exception(self):
        """
        Verify missing_cover raises an exception if it gets bad response
        from Patchwork.
        """
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'
        responses.add(responses.GET, patch_api_url, status=404)

        with self.assertRaises(Exception):
            patch_trigger.missing_cover(
                patchwork_url,
                SeriesData(patches=['http://patchwork.test/123'])
            )

    def test_cover_present(self):
        """Verify missing_cover() returns False if a cover is present."""
        self.assertFalse(
            patch_trigger.missing_cover('patchwork', SeriesData(cover='link'))
        )

    @responses.activate
    def test_get_patch_not_missing_cover(self):
        """
        Verify missing_cover() returns False if a cover is not expected.
        """
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'

        responses.add(responses.GET,
                      patch_api_url,
                      status=200,
                      json={'headers': {'In-Reply-To': 'msgid-of-cover'}})

        self.assertTrue(patch_trigger.missing_cover(
            patchwork_url,
            SeriesData(patches=[patchwork_url + '/patch/123'])
        ))

    @responses.activate
    def test_get_patch_missing_cover(self):
        """
        Verify missing_cover() returns True if a cover is expected.
        """
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'

        responses.add(responses.GET,
                      patch_api_url,
                      status=200,
                      json={'headers': {}})

        self.assertFalse(patch_trigger.missing_cover(
            patchwork_url,
            SeriesData(patches=[patchwork_url + '/patch/123'])
        ))


class TestRetrySkippedSeries(unittest.TestCase):
    """Test cases for patch_trigger.retry_skipped()."""
    @mock.patch('triggers.patch_trigger.get_series_data')
    def test_normal_retry(self, mock_series_data):
        series_data = {'series': 'data'}
        mock_series_data.return_value = series_data

        self.assertEqual(patch_trigger.retry_skipped('patchwork', [123]),
                         [series_data])
        mock_series_data.assert_called_once()


class TestSeriesByPatch(unittest.TestCase):
    """Tests for patch_trigger.get_series_by_patch()."""
    @responses.activate
    @mock.patch('triggers.patch_trigger.get_series_data')
    def test_get_series_by_patch(self, mock_get_series_data):
        """
        Verify the functionality of get_series_by_patch(). Check already seen
        patches/series are not retrieved again and data returned by
        get_series_data is returned.
        """
        patchwork_url = 'http://patchwork.test'
        patch_endpoint = patchwork_url + '/api/patches?filters'
        get_series_data_return = {'patches': [123]}
        mock_get_series_data.return_value = get_series_data_return

        responses.add(
            responses.GET,
            patch_endpoint,
            status=200,
            json=[{'id': 123, 'series': [{'id': 1}]}],
            headers={'Link': f'<{patchwork_url}/api/patches>; rel="next"'})
        responses.add(responses.GET,
                      patch_endpoint,
                      status=200,
                      json={'id': 123, 'series': [{'id': 1}]})

        returned_data = patch_trigger.get_series_by_patch(patchwork_url,
                                                          patch_endpoint)
        self.assertEqual(returned_data, [get_series_data_return])
        mock_get_series_data.assert_called_once()

    @responses.activate
    def test_get_series_by_patch_exception(self):
        """
        Verify get_series_by_patch raises an exception if it gets bad response
        from Patchwork.
        """
        patch_api_url = 'http://patchwork.test/api/patches'
        responses.add(responses.GET, patch_api_url, status=404)

        with self.assertRaises(Exception):
            patch_trigger.get_series_by_patch('patchwork', patch_api_url)


class TestGetSeriesData(unittest.TestCase):
    """Tests for patch_trigger.get_series_data()."""
    @responses.activate
    @mock.patch('triggers.patch_trigger.sanitize_emails')
    @mock.patch('triggers.patch_trigger.get_patch_data')
    def test_get_series_data(self, mock_patch_data, mock_emails):
        """Verify the functionality of get_series_data."""
        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/1'
        mock_emails.return_value = set()
        mock_patch_data.return_value = ('message_id', 'subject', 'date')
        series_data = {'id': 123, 'name': '[series]', 'received_all': True,
                       'cover_letter': None, 'patches': [
                           {'id': 123, 'name': '[patch]', 'web_url': 'url'}
                       ]}
        expected_series = SeriesData(
            patches=['url'],
            emails=set(),
            last_tested='date',
            message_id='message_id',
            subject='subject',
            series_id=str(123)
        )

        responses.add(responses.GET,
                      series_url,
                      status=200,
                      json=series_data)
        responses.add(responses.GET,
                      patchwork_url + '/api/patches/123',
                      status=200,
                      json={'id': 123, 'series': [{'id': 1}], 'headers': {}})

        return_value = patch_trigger.get_series_data(patchwork_url, series_url)
        self.assertEqual(return_value, expected_series)
        mock_patch_data.assert_called_once()
        mock_emails.assert_called_once()

    @responses.activate
    def test_get_series_data_exception(self):
        """
        Verify get_series_data raises an exception if Patchwork returns bad
        response. Checks both series and patch retrieval.
        """
        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/'

        responses.add(responses.GET, series_url + '1', status=400)
        # Check for exception with bad series retrieval
        with self.assertRaises(Exception):
            patch_trigger.get_series_data(patchwork_url, series_url + '1')

        series_data = {'id': 123, 'name': '[series]', 'received_all': True,
                       'cover_letter': None, 'patches': [
                           {'id': 123, 'name': '[patch]', 'web_url': 'url'}
                       ]}
        responses.add(responses.GET,
                      series_url + '2',
                      status=200,
                      json=series_data)
        responses.add(responses.GET,
                      patchwork_url + '/api/patches/123',
                      status=500)
        # Check for exception with bad patch retrieval
        with self.assertRaises(Exception):
            patch_trigger.get_series_data(patchwork_url, series_url + '2')

    @responses.activate
    def test_get_series_skip_series(self):
        """
        Verify get_series_data skips series if they contain one of PATCH_SKIP
        patterns in the name or aren't completed.
        """
        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/1'
        series_data = {'id': 123, 'name': '[series]', 'received_all': False}

        responses.add(responses.GET, series_url, status=200, json=series_data)
        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url),
                         None)

        series_data = {'id': 123, 'name': '[ethtool]', 'received_all': True}
        responses.add(responses.GET, series_url, status=200, json=series_data)

        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url),
                         None)

    @responses.activate
    @mock.patch('triggers.patch_trigger.sanitize_emails')
    @mock.patch('triggers.patch_trigger.get_patch_data')
    def test_get_series_skip_patches(self, mock_patch_data, mock_emails):
        """
        Verify get_series_data skips individual patches if they contain one of
        PATCH_SKIP patterns in the name.
        """
        mock_emails.return_value = set()
        mock_patch_data.return_value = ('message_id', 'subject', 'date')

        patchwork_url = 'http://patchwork.test'
        series_url = patchwork_url + '/api/series/'
        series_data = {'id': 123, 'name': '[series]', 'received_all': True,
                       'cover_letter': {'mbox': 'cover_mbox'}, 'patches': [
                           {'id': 123, 'name': '[iproute]', 'web_url': 'url'}
                       ]}
        responses.add(responses.GET,
                      series_url + '1',
                      status=200,
                      json=series_data)

        # Series contain a single patch that should be skipped
        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url + '1'),
                         None)

        series_data['patches'].append(
            {'id': 124, 'name': 'kernelpatch', 'web_url': 'url2'}
        )
        responses.add(responses.GET,
                      series_url + '2',
                      status=200,
                      json=series_data)
        responses.add(responses.GET,
                      patchwork_url + '/api/patches/124',
                      status=200,
                      json={'headers': {}})

        # Only the second patch data should be returned
        self.assertEqual(patch_trigger.get_series_data(patchwork_url,
                                                       series_url + '2'),
                         SeriesData(patches=['url2'],
                                    last_tested='date',
                                    message_id='message_id',
                                    subject='subject',
                                    cover='cover_mbox',
                                    series_id=str(123)))
        mock_patch_data.assert_called_once()


class TestGetPatchData(unittest.TestCase):
    """Tests for patch_trigger.get_patch_data()."""
    @responses.activate
    def test_patch_data_exception(self):
        """
        Verify get_patch_data raises an exception if Patchwork returns bad
        response.
        """
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'
        patch_web_url = patchwork_url + '/patch/123'

        responses.add(responses.GET, patch_api_url, status=400)
        with self.assertRaises(Exception):
            patch_trigger.get_patch_data(patchwork_url, patch_web_url)

    @responses.activate
    def test_patch_data_valid(self):
        """Verify get_patch_data works with valid data."""
        patchwork_url = 'http://patchwork.test'
        patch_api_url = patchwork_url + '/api/patches/123'
        patch_web_url = patchwork_url + '/patch/123'
        data = {'headers': {'Subject': '[subject]', 'Message-ID': '<ab@cd>'},
                'id': 123,
                'date': 'date'}

        responses.add(responses.GET, patch_api_url, status=200, json=data)
        self.assertEqual(patch_trigger.get_patch_data(patchwork_url,
                                                      patch_web_url),
                         ('<ab@cd>', '[subject]', 'date'))


class TestSanitizeEmails(unittest.TestCase):
    """Tests for patch_trigger.sanitize_emails()."""
    def test_missing(self):
        """Verify sanitize_emails works even if there are no such headers."""
        self.assertEqual(set(), patch_trigger.sanitize_emails({}))

    def test_with_data(self):
        headers = {'HeaderX': 'invalid', 'From': 'Name Surname <name@email>',
                   'To': 'Alias <name@email>, someoneelse@email'}
        self.assertEqual(set(['name@email', 'someoneelse@email']),
                         patch_trigger.sanitize_emails(headers))


class TestLoadTriggers(unittest.TestCase):
    """Tests for patch_trigger.load_triggers()."""
    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.patch_trigger.get_next_date_and_skipped_series')
    @mock.patch('triggers.patch_trigger.get_new_series')
    @mock.patch('triggers.patch_trigger.missing_cover')
    def test_set_values(self, mock_cover, mock_series, mock_date_series,
                        mock_commit):
        """
        Verify all required keys and values are set and renamed if the config
        is valid.
        """
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branch: master\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
            '  patchwork:\n'
            '    url: http://patchwork.url\n'
            '    project: patchwork-project\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_cover.return_value = False
        mock_commit.return_value = 'baseline_ref'
        mock_date_series.return_value = (
            dateutil.parser.parse('2018-07-19T00:45:18'),
            []
        )
        mock_series.return_value = [SeriesData(
            patches=['http://patchwork.url/patch/123'],
            emails=set(['name@email']),
            subject='subject',
            message_id='message_id',
            last_tested='2018-07-19T10:45:18',
            series_id=str(123),
            cover='http://patchwork.url/cover/123/mbox'
        )]

        expected_trigger = {
            'baserepo': 'http://git.test/git/kernel.git',
            'patchwork_url': 'http://patchwork.url',
            'patchwork_project': 'patchwork-project',
            'cki_pipeline_type': 'patchwork',
            'name': 'test_name',
            'ref': 'baseline_ref',
            'branch': 'master',
            'cki_pipeline_branch': 'test_name',
            'cki_project': 'username/project',
            'patchwork': 'http://patchwork.url/patch/123',
            'subject': 'Re: subject',
            'message_id': 'message_id',
            'send_report_to_upstream': 'False',
            'send_report_on_success': 'False',
            'date': '2018-07-19T10:45:18',
            'mail_to': '',
            'make_target': 'targz-pkg',
            'cover_letter': 'http://patchwork.url/cover/123/mbox',
            'skipped_series': '',
            'title': 'Patch: test_name: subject'
        }

        trigger = patch_trigger.load_triggers(gitlab, config)[0]
        self.maxDiff = None
        self.assertDictEqual(trigger, expected_trigger)

        config_text += '  send_report_to_upstream: True\n'
        config = yaml.safe_load(config_text)
        trigger = patch_trigger.load_triggers(gitlab, config)[0]
        expected_trigger['send_report_to_upstream'] = "True"
        expected_trigger['mail_to'] = ', name@email'
        self.assertDictEqual(trigger, expected_trigger)

        config_text += '  mail_to: foo@bar.com\n'
        config = yaml.safe_load(config_text)
        expected_trigger['mail_to'] = 'foo@bar.com, name@email'
        trigger = patch_trigger.load_triggers(gitlab, config)[0]
        self.assertDictEqual(trigger, expected_trigger)

    @mock.patch('triggers.utils.get_commit_hash')
    @mock.patch('triggers.patch_trigger.get_next_date_and_skipped_series')
    @mock.patch('triggers.patch_trigger.get_new_series')
    @mock.patch('triggers.patch_trigger.missing_cover')
    def test_missing_series(self, mock_cover, mock_series, mock_date_series,
                            mock_commit):
        """
        Verify newly missing series are correctly assigned and triggers for
        them are not submitted.
        """
        config_text = '{}'.format(
            'test_name:\n'
            '  git_url: http://git.test/git/kernel.git\n'
            '  branch: master\n'
            '  cki_project: username/project\n'
            '  cki_pipeline_branch: test_name\n'
            '  patchwork:\n'
            '    url: http://patchwork.url\n'
            '    project: patchwork-project\n'
        )
        config = yaml.safe_load(config_text)

        gitlab = fakes.FakeGitLab()
        gitlab.add_project('username/project')

        mock_cover.side_effect = [True, False]
        mock_commit.return_value = 'baseline_ref'
        mock_date_series.return_value = (
            dateutil.parser.parse('2018-07-19T00:45:18'),
            []
        )
        mock_series.return_value = [
            SeriesData(patches=['http://patchwork.url/patch/123'],
                       emails=set(['name@email']),
                       subject='subject',
                       message_id='message_id',
                       last_tested='2018-07-19T10:45:18',
                       series_id=str(123)),
            SeriesData(patches=['http://patchwork.url/patch/124'],
                       emails=set(['name2@email']),
                       subject='subject2',
                       message_id='message_id2',
                       last_tested='2018-07-20T10:45:18',
                       series_id=str(124))
        ]

        triggers = patch_trigger.load_triggers(gitlab, config)
        self.assertEqual(len(triggers), 1)
        self.assertEqual('123', triggers[0]['skipped_series'])
