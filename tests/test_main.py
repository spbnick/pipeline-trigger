import argparse
import os
import unittest

import mock

import triggers.__main__ as main_module

import fakes


class TestMain(unittest.TestCase):
    """Tests for triggers.__main__.main()."""
    @mock.patch.dict('triggers.TRIGGERS', {'fake-trigger': fakes})
    @mock.patch('gitlab.Gitlab')
    @mock.patch('builtins.open', new_callable=mock.mock_open, read_data='test')
    @mock.patch('argparse.ArgumentParser.parse_args')
    @mock.patch('triggers.__main__.trigger_pipelines')
    def test_main_calls(self, mock_trigger, mock_argparse, mock_file,
                        mock_gitlab):
        """Verify main() calls all the functions it should."""
        mock_gitlab.return_value = fakes.FakeGitLab()
        mock_argparse.return_value = argparse.Namespace(
            trigger='fake-trigger', config=None, token='GITLAB_TRIGGER_TOKEN'
        )
        mock_trigger.return_value = 'something'

        os.environ.update({'GITLAB_URL': 'http://gitlab.test',
                           'GITLAB_PRIVATE_TOKEN': 'private-token',
                           'GITLAB_TRIGGER_TOKEN': 'trigger-token'})

        main_module.main()

        mock_trigger.assert_called_once()

        # Cleanup
        del os.environ['GITLAB_URL']
        del os.environ['GITLAB_PRIVATE_TOKEN']
        del os.environ['GITLAB_TRIGGER_TOKEN']
