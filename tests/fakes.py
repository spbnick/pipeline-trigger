"""Fake classes."""


def load_triggers(gitlab_instance, config):
    return [{'trigger': 'fake'}]


class FakeConnection():
    def __init__(self):
        self._headers = {}

    def putheader(self, key, value):
        self._headers[key] = value

    def get_headers(self):
        return self._headers


class WrapMe():
    """The weird XMLRPC interface that eats version first."""
    def __init__(self):
        pass

    def test_me(version, *args, **kwargs):
        return (version, 'value')


class FakeServerProxy():
    def __init__(self):
        self._projects = []
        self._patches = []

    def add_patch(self, data):
        self._patches.append(data)

    def add_project(self, data):
        self._projects.append(data)

    def __filter_patch(self, patch, project_id, patch_id):
        if patch['project_id'] == project_id and patch['id'] > patch_id:
            return True
        return False

    def project_list(self, project_name):
        return list(filter(lambda project: project_name in project['linkname'],
                           self._projects))

    def patch_list(self, filters, archived, fields):
        return list(filter(
            lambda patch: self.__filter_patch(
                patch, filters['project_id'], filters['id__gt']
            ),
            self._patches
        ))


class FakeGitLabCommits():
    def __init__(self):
        self._commits = []

    def create(self, data):
        self._commits.append(data)

    def __getitem__(self, index):
        return self._commits[index]


class FakePipelineJob():
    def __init__(self, job_id):
        self.attributes = {'id': job_id}


class FakePipelineJobs():
    def __init__(self):
        self._jobs = []
        self.add_job(1)

    def add_job(self, job_id):
        self._jobs.append(FakePipelineJob(job_id))

    def list(self, page=None, per_page=None):
        return self._jobs


class FakePipeline():
    def __init__(self, branch, token, variables, status):
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status}
        for key, value in variables.items():
            self.attributes[key] = value

        self.branch = branch
        self.token = token
        self.variables = variables


class FakeProjectPipelines():
    def __init__(self):
        self._pipelines = []

    def add_new_pipeline(self, branch, token, variables, status):
        self._pipelines.append(
            FakePipeline(branch, token, variables, status)
        )

    def __getitem__(self, index):
        return self._pipelines[index]

    def list(self, page=None, per_page=None):
        return self._pipelines


class FakeGitLabProject():
    def __init__(self):
        self.commits = FakeGitLabCommits()
        self.attributes = {'web_url': 'http://web-url'}
        self.pipelines = FakeProjectPipelines()

    def trigger_pipeline(self, branch, token, variables, status='pending'):
        self.pipelines.add_new_pipeline(branch, token, variables, status)


class FakeGitLab():
    def __init__(self):
        self.projects = {}

    def add_project(self, project_name):
        self.projects[project_name] = FakeGitLabProject()
